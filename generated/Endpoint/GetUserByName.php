<?php

declare(strict_types=1);

/*
 * This file has been auto generated by Jane,
 *
 * Do no edit it directly.
 */

namespace MyClientSncf\Api\Endpoint;

class GetUserByName extends \Jane\OpenApiRuntime\Client\BaseEndpoint implements \Jane\OpenApiRuntime\Client\Psr7HttplugEndpoint
{
    protected $username;

    /**
     * @param string $username The name that needs to be fetched. Use user1 for testing.
     */
    public function __construct(string $username)
    {
        $this->username = $username;
    }

    use \Jane\OpenApiRuntime\Client\Psr7HttplugEndpointTrait;

    public function getMethod(): string
    {
        return 'GET';
    }

    public function getUri(): string
    {
        return str_replace(['{username}'], [$this->username], '/user/{username}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, \Http\Message\StreamFactory $streamFactory = null): array
    {
        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \MyClientSncf\Api\Exception\GetUserByNameBadRequestException
     * @throws \MyClientSncf\Api\Exception\GetUserByNameNotFoundException
     *
     * @return \MyClientSncf\Api\Model\User|null
     */
    protected function transformResponseBody(string $body, int $status, \Symfony\Component\Serializer\SerializerInterface $serializer)
    {
        if (200 === $status) {
            return $serializer->deserialize($body, 'MyClientSncf\\Api\\Model\\User', 'json');
        }
        if (400 === $status) {
            throw new \MyClientSncf\Api\Exception\GetUserByNameBadRequestException();
        }
        if (404 === $status) {
            throw new \MyClientSncf\Api\Exception\GetUserByNameNotFoundException();
        }
    }
}
