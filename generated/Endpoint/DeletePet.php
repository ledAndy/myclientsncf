<?php

declare(strict_types=1);

/*
 * This file has been auto generated by Jane,
 *
 * Do no edit it directly.
 */

namespace MyClientSncf\Api\Endpoint;

class DeletePet extends \Jane\OpenApiRuntime\Client\BaseEndpoint implements \Jane\OpenApiRuntime\Client\Psr7HttplugEndpoint
{
    protected $petId;

    /**
     * @param int   $petId            Pet id to delete
     * @param array $headerParameters {
     *
     *     @var string $api_key
     * }
     */
    public function __construct(int $petId, array $headerParameters = [])
    {
        $this->petId = $petId;
        $this->headerParameters = $headerParameters;
    }

    use \Jane\OpenApiRuntime\Client\Psr7HttplugEndpointTrait;

    public function getMethod(): string
    {
        return 'DELETE';
    }

    public function getUri(): string
    {
        return str_replace(['{petId}'], [$this->petId], '/pet/{petId}');
    }

    public function getBody(\Symfony\Component\Serializer\SerializerInterface $serializer, \Http\Message\StreamFactory $streamFactory = null): array
    {
        return [[], null];
    }

    public function getExtraHeaders(): array
    {
        return ['Accept' => ['application/json']];
    }

    protected function getHeadersOptionsResolver(): \Symfony\Component\OptionsResolver\OptionsResolver
    {
        $optionsResolver = parent::getHeadersOptionsResolver();
        $optionsResolver->setDefined(['api_key']);
        $optionsResolver->setRequired([]);
        $optionsResolver->setDefaults([]);
        $optionsResolver->setAllowedTypes('api_key', ['string']);

        return $optionsResolver;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \MyClientSncf\Api\Exception\DeletePetBadRequestException
     * @throws \MyClientSncf\Api\Exception\DeletePetNotFoundException
     */
    protected function transformResponseBody(string $body, int $status, \Symfony\Component\Serializer\SerializerInterface $serializer)
    {
        if (400 === $status) {
            throw new \MyClientSncf\Api\Exception\DeletePetBadRequestException();
        }
        if (404 === $status) {
            throw new \MyClientSncf\Api\Exception\DeletePetNotFoundException();
        }
    }
}
